#include <cassert>

#include "alsapkgfxdb.h"

#define CSTR(str) ((str).c_str())

bool mk_pkg(const std::string & pkg_name, const std::string & req)
{
    return !system(CSTR("mkdir -p " + pkg_name +"/src/script/script2 2> /dev/null")) &&
    !system(CSTR("echo \'echo 'Hello World 2'\' > " + pkg_name + "/src/script/exe")) &&
    !system(CSTR("echo \'echo 'Hello World 3'\' > " + pkg_name + "/src/script/script2/exe")) &&
    !system(CSTR("echo \'echo 'Hello World'\' > " + pkg_name + "/src/exe")) &&
    !system(CSTR("echo \'/tmp\' > " + pkg_name + "/prefix.txt")) &&
    !system(CSTR("echo -e '" + req + "' > mypkg/requirements.txt")) &&
    !system(CSTR("echo \'echo Uninstall script activated\' > " + pkg_name + "/uninstall.sh")) &&
    !system(CSTR("echo \'echo Install script activated\' > " + pkg_name + "/install.sh")) &&
    !system(CSTR("chmod +x " + pkg_name
        + "/install.sh " + pkg_name + "/uninstall.sh "
        + pkg_name + "/src/exe " + pkg_name + "/src/script/exe"));
}

int main()
{
    assert(mk_pkg("PkgConfig", ""));
    assert(mk_pkg("mypkg", "PkgConfig"));

    assert(pkg_pack("PkgConfig/"));
    assert(pkg_pack("mypkg/"));

    assert(pkg_install("PkgConfig.pkg", "", "/tmp/"));

    return 0;
}

