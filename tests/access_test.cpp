#include "alsapkgfxdb.h"
#include <cstdlib>
#include <cassert>

int main()
{
    system("mkdir /tmp/pkg_test/ 2> /dev/null");
    system(R"(echo -e "File1\nFile2\nFile 3. fucking extensions" > /tmp/pkg_test/filelist.txt 2> /dev/null)");
    system("echo / > /tmp/pkg_test/prefix.txt 2> /dev/null");

    auto pkg = access("pkg_test", "/tmp/");

    assert(pkg.pkg_state == 0);
    assert(pkg.uninstall.empty());
    assert(pkg.file_list);
    assert(pkg.prefix == "/");

    return 0;
}
