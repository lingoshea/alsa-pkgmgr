#include "alsapkgfxdb.h"
#include <cstdlib>
#include <cassert>
#include <fcntl.h>           /* Definition of AT_* constants */
#include <unistd.h>

int main()
{
    system("mkdir -p mypkg/src/script/script2 2> /dev/null");
    system("echo \'echo 'Hello World 2'\' > mypkg/src/script/exe");
    system("echo \'echo 'Hello World 3'\' > mypkg/src/script/script2/exe");
    system("echo \'echo 'Hello World'\' > mypkg/src/exe");
    system("echo \'/tmp\' > mypkg/prefix.txt");
    system(R"(echo -e 'dumnass-shit-12.138\nlinux-1991.version1' > mypkg/requirements.txt)");
    system("echo \'echo Uninstall script activated\' > mypkg/uninstall.sh");
    system("echo \'echo Install script activated\' > mypkg/install.sh");
    system("chmod +x mypkg/install.sh mypkg/uninstall.sh mypkg/src/exe mypkg/src/script/exe");

    assert(pkg_pack("mypkg/"));
    assert(!access("mypkg.pkg", F_OK));

    return 0;
}
