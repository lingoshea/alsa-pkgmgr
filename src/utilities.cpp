#include <cstring>
#include <algorithm>

#include "utilities.h"
#include "alsapkgfxdb.h"

bool ls(const char *path, const char * filelist_path)
{
    struct dirent* dp = nullptr;
    DIR  * dir  = opendir(path);
    std::ofstream filelist(filelist_path);

    // Unable to open directory stream
    if (!dir || !filelist)
    {
        return false;
    }

    if (!show(filelist, dir, dp, path))
    {
        return false;
    }

    // Close directory stream
    closedir(dir);
    filelist.close();

    return true;
}

std::vector < std::string > read_dependency_info(const std::string & root_dir)
{
    std::vector < std::string > dependency;
    // resolve dependency
    std::ifstream dependency_file(root_dir + "requirements.txt");
    if (dependency_file)
    {
        std::string req_pkg_name;
        while (dependency_file)
        {
            getline(dependency_file, req_pkg_name);
            if (!req_pkg_name.empty())
            {
                dependency.push_back(req_pkg_name);
            }
        }
        dependency_file.close();
    }

    return dependency;
}

bool assemble_dep_info(const std::vector < std::string > & dependency,
    std::ofstream & pkg_file, const std::string & pkg_name)
{

    uint64_t list_size = dependency.size();

    ON_ERROR(
        pkg_file.write(msg_head, strlen(msg_head) + sizeof(char) /* write including the EOF indicator */),
        return false)

    uint64_t name_len = pkg_name.length();
    ON_ERROR(pkg_file.write(MAKE_PARAM_FOR_NUM(name_len)), return false)
    ON_ERROR(pkg_file.write(pkg_name.c_str(), pkg_name.length()), return false)

    ON_ERROR(
        pkg_file.write(MAKE_PARAM_FOR_NUM(list_size)),
    return false)

    for (const auto& pkg_dep : dependency)
    {
        uint64_t str_size = pkg_dep.length();

        ON_ERROR(
            pkg_file.write(MAKE_PARAM_FOR_NUM(str_size)),
        return false)

        ON_ERROR(
            pkg_file.write(pkg_dep.c_str(), pkg_dep.length()),
        return false)
    }

    return true;
}

bool show(std::ofstream& filelist, DIR  * cur_dir, struct dirent * dp,
    const std::string& prefix,
    const std::string& root)
{
    while ((dp = readdir(cur_dir)) != nullptr)
    {
        std::string name = dp->d_name;
        if (dp->d_type == DT_DIR)
        {
            if (strcmp(dp->d_name, ".") == 0
                || strcmp(dp->d_name, "..") == 0)
            {
                // ignore
                continue;
            }
            else
            {
                auto new_cur_dir = opendir ((prefix + root + "/" + dp->d_name).c_str());
                if (!show(filelist, new_cur_dir, dp, prefix, root + "/" + dp->d_name))
                {
                    return false;
                }
                closedir(new_cur_dir);
            }
        }

        if (!(filelist << root + "/" + dp->d_name << std::endl))
        {
            filelist.close();
            return false;
        }
    }

    return true;
}

bool unpack_verify(std::ifstream & pkg_file)
{
    int msg_len = strlen(msg_head);
    char buff[16]; // should be more than enough
    pkg_file.read(buff, msg_len + 1 /* read including EOF indicator*/);
    return !strcmp(buff, msg_head);
}

std::vector < std::string >
read_dependency_info(std::ifstream & pkg_file, bool & state)
{
    std::vector < std::string > dependency;

    uint64_t list_size;
    ON_ERROR(
        pkg_file.read(MAKE_PARAM_FOR_NUM(list_size)), {
            state = false;
            return dependency;
        })

    for (uint64_t i = 0; i < list_size; i++)
    {
        uint64_t str_size;
        char buff[MAX_NON_SUSPICIOUS_STR_SIZE] = { };

        ON_ERROR(
            pkg_file.read(MAKE_PARAM_FOR_NUM(str_size)), {
            state = false;
            return dependency;
        })

        ON_ERROR(
            str_size <= MAX_NON_SUSPICIOUS_STR_SIZE, {
            state = false;
            return dependency;
        })

        ON_ERROR(
            pkg_file.read(buff, str_size),{
            state = false;
            return dependency;
        })

        dependency.emplace_back(buff);
    }

    return dependency;
}

std::string get_pkg_name_from_path(const std::string& root_dir)
{
    auto pos = root_dir.find_last_of('/',
        root_dir.length() - 2 /* offset starts with 0, offset -1 is the actual pos*/);

    if (pos != std::string::npos)
    {
        return root_dir.substr(pos + 1);
    }

    return root_dir.substr(0, root_dir.length() - 1);
}

bool unpack(std::ifstream & pkg_file, const std::string& tmp_path)
{
    const char * tmp_file_name = "pkg_unpack";
    const std::string tmp_file_storage_path = tmp_path + "tmp/";

    system(("mkdir -p " + tmp_file_storage_path).c_str());

    std::string tmp_file_path = tmp_file_storage_path + std::string(tmp_file_name);
    std::ofstream pkg_tar_file(tmp_file_path);

    char buff[NOR_IO_BUFF_SIZE];

    while ( const auto& data_stream = pkg_file.read(buff, sizeof(buff)) )
    {
        ON_ERROR(
            pkg_tar_file.write(buff, data_stream.gcount()),
            return false;
        )
    }

    pkg_tar_file.close();
    pkg_file.close();

    return !system(("tar -xf " + tmp_file_path + " -C " + tmp_file_storage_path).c_str());
}

std::string get_pkg_name_from_file(std::ifstream & pkg_file)
{
    char buff[MAX_NON_SUSPICIOUS_STR_SIZE] = { };
    uint64_t name_len;
    ON_ERROR(pkg_file.read(MAKE_PARAM_FOR_NUM(name_len)), return "")
    ON_ERROR(pkg_file.read(buff, name_len), return "");

    return buff;
}

std::string get_prefix_from_path(const std::string& root_dir)
{
    std::string buff;
    std::ifstream prefix_file(root_dir + "/prefix.txt");

    if (!prefix_file)
    {
        return DEFAULT_INSTALL_PATH;
    }
    else
    {
        ON_ERROR (getline(prefix_file, buff),
            return DEFAULT_INSTALL_PATH)
        prefix_file.close();
        return buff;
    }
}
