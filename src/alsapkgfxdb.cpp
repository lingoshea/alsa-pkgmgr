#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <vector>

#include "alsapkgfxdb.h"
#include "utilities.h"

pkg_db_t access(const std::string& pkg_name, const std::string& db_folder)
{
    pkg_db_t ret = { .pkg_state = 0 };
    struct stat dir_state {};

    int err = stat((db_folder + pkg_name).c_str(), &dir_state);

    if(err != -1 && S_ISDIR(dir_state.st_mode))
    {
        // open file list
        ret.file_list.open(db_folder + pkg_name + "/filelist.txt", std::ios::in);

        // get file prefix
        std::ifstream file_prefix(db_folder + pkg_name + "/prefix.txt");
        if (file_prefix)
        {
            getline(file_prefix, ret.prefix);
            file_prefix.close();
        }
        else
        {
            ret.prefix = "/usr/local";
        }

        // check uninstall.sh file
        std::string file_uninstall_path = db_folder + pkg_name + "/uninstall.sh";
        if (!::access(file_uninstall_path.c_str(), R_OK))
        {
            ret.uninstall = file_uninstall_path;
        }

        return ret;
    }
    else
    {
        if (!errno)
        {
            return { .pkg_state = -1 };
        }
        else
        {
            return { .pkg_state = -errno };
        }
    }
}

bool pkg_pack (const std::string & root_dir)
{
    std::vector < std::string > dependency;
    const std::string pkg_name = get_pkg_name_from_path(root_dir);

    if (!system(nullptr)) // no console available
    {
        return false;
    }

    // tar -xf src.txz PKGROOT/src
    std::string src_path = root_dir + "src";
    std::string filelist_path = root_dir + "filelist.txt";
    std::string compression_command = "tar -kcf " + root_dir + "src.txz -I \"xz -T $(nproc)\" -C " + src_path + " .";
    std::string pack_command = "tar --exclude=" + src_path + " -kcf " + "." + pkg_name + ".pkg " + root_dir;

    if (!ls(src_path.c_str(), filelist_path.c_str()))
    {
        return false;
    }

    int compression_result = system(compression_command.c_str());
    int packing_operation_result = system(pack_command.c_str());

    dependency = read_dependency_info(root_dir);

    // write dependency info
    if (!compression_result && !packing_operation_result)
    {
        std::ofstream pkg_file(pkg_name + ".pkg", std::ios::binary);
        std::ifstream tar_pkg_file("." + pkg_name + ".pkg", std::ios::binary);
        if (!pkg_file)
        {
            return false;
        }

        bool assemble_result = assemble_dep_info(dependency, pkg_file, pkg_name);
        if (assemble_result)
        {
            char buffer [NOR_IO_BUFF_SIZE];
            while (  const auto & data_stream = tar_pkg_file.read ( buffer, sizeof(buffer) )  )
            {
                ON_ERROR(pkg_file.write(buffer, data_stream.gcount()),{
                    assemble_result = false;
                    break;
                })
            }
        }

        pkg_file.close();
        tar_pkg_file.close();

        return !remove(("." + pkg_name + ".pkg").c_str()) && assemble_result;
    }
    else
    {
        return false;
    }
}

bool pkg_install(const std::string & pkg_path,
    std::string prefix,
    const std::string& db_path)
{
    system(("mkdir -p " + db_path + "db/").c_str());

    std::ifstream pkg_file (pkg_path);
    const std::string tmp_file_storage_path = db_path + "tmp/";

    if (!unpack_verify(pkg_file) || !system(nullptr))
    {
        return false;
    }

    auto pkg_name = get_pkg_name_from_file(pkg_file);

    bool state = true;
    auto dependency_list = read_dependency_info(pkg_file, state);
    if (!state)
    {
        return false;
    }

    // FIXME:: transaction check here

    // no conflicting then, install the package
    ON_ERROR(unpack(pkg_file, db_path), return false)

    if (prefix.empty())
    {
        prefix = get_prefix_from_path(tmp_file_storage_path + pkg_name);
    }

    system(("mkdir -p " + prefix).c_str());

    bool user_install_script_result = !system((tmp_file_storage_path + pkg_name + "/install.sh").c_str());

    bool unpacking_result =
        !system(("tar --no-overwrite-dir -xf " + tmp_file_storage_path + pkg_name + "/src.txz -C " + prefix).c_str());

    std::string needed_files [] = {"uninstall.sh", "prefix.txt", "filelist.txt"};
    bool copy_result = true;
    for (const auto & file :  needed_files)
    {
        copy_result = !system(("mkdir -p " + db_path + "db/" + pkg_name).c_str()) && copy_result;
        copy_result = !system(("cp "
            + tmp_file_storage_path + pkg_name + "/" + file + " "
            + db_path + "db/" + pkg_name).c_str()) && copy_result;
    }

    auto remove_tmp_files_result =
        !system(("rm -rf "  + tmp_file_storage_path + pkg_name).c_str());

    if (!copy_result)
    {
        system(("rm -rf " + db_path + "db/" + pkg_name).c_str());
    }

    return user_install_script_result
        && unpacking_result
        && copy_result
        && remove_tmp_files_result;
}
