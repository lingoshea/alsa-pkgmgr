#ifndef _ALSAPKGFXDB_H_
#define _ALSAPKGFXDB_H_

#include <string>
#include <fstream>
#include <unordered_map>
#include <vector>

#define DEFAULT_DB_PATH    "/var/alsapkgfxdb/db/"
#define DEFAULT_PATH       "/var/alsapkgfxdb/"
#define DEFAULT_INSTALL_PATH "/usr/local"

struct pkg_db_t
{
    pkg_db_t() = default;

    int          pkg_state{};  // if the package can be found
    std::fstream file_list;  // list of all files installed
    std::string  prefix;     // prefix of installation path
    std::string  uninstall;  // path to uninstall shell script
                             // should be run before uninstalling
};

enum FILE_TRASACTION_ERROR_T { FILE_CONFLICT };

typedef std::unordered_map
    < FILE_TRASACTION_ERROR_T /* error_type */,
    std::string /* detailed info */ >
transaction_error_t;

struct transaction_check_t
{
    std::unordered_map < std::string /* failed file */, transaction_error_t /* reason */ > failed_list;
    // failed list is clear means transaction check successful
};

// pack the package
bool pkg_pack(const std::string & root_dir /* !! REQUIRE '/' at the end !! */);

// install a package
bool pkg_install(const std::string & pkg_path,
    std::string prefix  = "", // "" for default, can be manually overridden
    const std::string& db_path = DEFAULT_DB_PATH);

transaction_check_t transaction_check(const std::string& pkg_path);

pkg_db_t access ( const std::string& pkg_name,
    const std::string& db_folder = DEFAULT_DB_PATH /* !! REQUIRE '/' at the end !! */ );

#endif //_ALSAPKGFXDB_H_
