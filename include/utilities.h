
#ifndef _UTILITIES_H_
#define _UTILITIES_H_

#include <vector>
#include <string>
#include <fstream>
#include <sys/types.h>
#include <dirent.h>

#include "alsapkgfxdb.h"

#define NOR_IO_BUFF_SIZE 4096
// auto make params for integer direct I/O
#define MAKE_PARAM_FOR_NUM(num) (char*)&(num), sizeof(num) / sizeof(char)

/* No package name will be longer than this value.
 * if so, then the file is corrupted
 */
#define MAX_NON_SUSPICIOUS_STR_SIZE 256

#define ON_ERROR(op, action) \
if (!(op)) \
{ \
    action; \
}

// add at the start of PKG file(including EOF indicator)
static const char * msg_head = "ALSAPKG";

// list all the files and dirs in a folder, write them to filelist_path
bool ls(const char *path, const char * filelist_path);

// read the dependency info in pkg directory when packing
std::vector < std::string >
    read_dependency_info(const std::string & root_dir);

// add msg_head and dependency info at the start of the file
bool assemble_dep_info(const std::vector < std::string > & dependency,
    std::ofstream & pkg_file, const std::string & pkg_name);

// a func for loop, recursively search all the files and dirs in a folder
bool show(std::ofstream& filelist, DIR  * cur_dir, struct dirent * dp,
    const std::string& prefix,
    const std::string& root = "");

// verify the pkg_file is indeed an A.L.S.A. Raw System Package
bool unpack_verify(std::ifstream & pkg_file);

// read the dependency info in a pkg file
std::vector < std::string >
read_dependency_info(std::ifstream & pkg_file, bool & state);

// unzip a package
bool unpack(std::ifstream & pkg_file,
    const std::string& tmp_path = DEFAULT_PATH "tmp/");

// get package name from path
std::string get_pkg_name_from_path(const std::string& root_dir);

// get package name in package file
std::string get_pkg_name_from_file(std::ifstream & pkg_file);

// get prefix
std::string get_prefix_from_path(const std::string& root_dir);

#endif //_UTILITIES_H_
